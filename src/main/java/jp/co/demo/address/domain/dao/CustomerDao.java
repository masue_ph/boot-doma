package jp.co.demo.address.domain.dao;

import java.util.List;

import org.seasar.doma.Dao;
import org.seasar.doma.Select;
import org.seasar.doma.boot.ConfigAutowireable;

import jp.co.demo.address.domain.model.Customer;

@Dao
@ConfigAutowireable
public interface CustomerDao {

	@Select
	List<Customer> selectAll();
}
