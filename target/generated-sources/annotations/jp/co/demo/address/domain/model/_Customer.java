package jp.co.demo.address.domain.model;

/** */
@javax.annotation.Generated(value = { "Doma", "2.6.2" }, date = "2016-11-01T15:36:11.771+0900")
public final class _Customer extends org.seasar.doma.jdbc.entity.AbstractEntityType<jp.co.demo.address.domain.model.Customer> {

    static {
        org.seasar.doma.internal.Artifact.validateVersion("2.6.2");
    }

    private static final _Customer __singleton = new _Customer();

    private final org.seasar.doma.jdbc.entity.NamingType __namingType = null;

    /** the id */
    public final org.seasar.doma.jdbc.entity.AssignedIdPropertyType<java.lang.Object, jp.co.demo.address.domain.model.Customer, java.lang.Integer, Object> $id = new org.seasar.doma.jdbc.entity.AssignedIdPropertyType<>(jp.co.demo.address.domain.model.Customer.class, java.lang.Integer.class, java.lang.Integer.class, () -> new org.seasar.doma.wrapper.IntegerWrapper(), null, null, "id", "", __namingType, false);

    /** the name */
    public final org.seasar.doma.jdbc.entity.DefaultPropertyType<java.lang.Object, jp.co.demo.address.domain.model.Customer, java.lang.String, Object> $name = new org.seasar.doma.jdbc.entity.DefaultPropertyType<>(jp.co.demo.address.domain.model.Customer.class, java.lang.String.class, java.lang.String.class, () -> new org.seasar.doma.wrapper.StringWrapper(), null, null, "name", "", __namingType, true, true, false);

    private final java.util.function.Supplier<org.seasar.doma.jdbc.entity.NullEntityListener<jp.co.demo.address.domain.model.Customer>> __listenerSupplier;

    private final boolean __immutable;

    private final String __catalogName;

    private final String __schemaName;

    private final String __tableName;

    private final boolean __isQuoteRequired;

    private final String __name;

    private final java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<jp.co.demo.address.domain.model.Customer, ?>> __idPropertyTypes;

    private final java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<jp.co.demo.address.domain.model.Customer, ?>> __entityPropertyTypes;

    private final java.util.Map<String, org.seasar.doma.jdbc.entity.EntityPropertyType<jp.co.demo.address.domain.model.Customer, ?>> __entityPropertyTypeMap;

    private _Customer() {
        __listenerSupplier = () -> ListenerHolder.listener;
        __immutable = false;
        __name = "Customer";
        __catalogName = "";
        __schemaName = "";
        __tableName = "customers";
        __isQuoteRequired = false;
        java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<jp.co.demo.address.domain.model.Customer, ?>> __idList = new java.util.ArrayList<>();
        java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<jp.co.demo.address.domain.model.Customer, ?>> __list = new java.util.ArrayList<>(2);
        java.util.Map<String, org.seasar.doma.jdbc.entity.EntityPropertyType<jp.co.demo.address.domain.model.Customer, ?>> __map = new java.util.HashMap<>(2);
        __idList.add($id);
        __list.add($id);
        __map.put("id", $id);
        __list.add($name);
        __map.put("name", $name);
        __idPropertyTypes = java.util.Collections.unmodifiableList(__idList);
        __entityPropertyTypes = java.util.Collections.unmodifiableList(__list);
        __entityPropertyTypeMap = java.util.Collections.unmodifiableMap(__map);
    }

    @Override
    public org.seasar.doma.jdbc.entity.NamingType getNamingType() {
        return __namingType;
    }

    @Override
    public boolean isImmutable() {
        return __immutable;
    }

    @Override
    public String getName() {
        return __name;
    }

    @Override
    public String getCatalogName() {
        return __catalogName;
    }

    @Override
    public String getSchemaName() {
        return __schemaName;
    }

    @Override
    public String getTableName() {
        return getTableName(org.seasar.doma.jdbc.Naming.DEFAULT::apply);
    }

    @Override
    public String getTableName(java.util.function.BiFunction<org.seasar.doma.jdbc.entity.NamingType, String, String> namingFunction) {
        if (__tableName.isEmpty()) {
            return namingFunction.apply(__namingType, __name);
        }
        return __tableName;
    }

    @Override
    public boolean isQuoteRequired() {
        return __isQuoteRequired;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void preInsert(jp.co.demo.address.domain.model.Customer entity, org.seasar.doma.jdbc.entity.PreInsertContext<jp.co.demo.address.domain.model.Customer> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.preInsert(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void preUpdate(jp.co.demo.address.domain.model.Customer entity, org.seasar.doma.jdbc.entity.PreUpdateContext<jp.co.demo.address.domain.model.Customer> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.preUpdate(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void preDelete(jp.co.demo.address.domain.model.Customer entity, org.seasar.doma.jdbc.entity.PreDeleteContext<jp.co.demo.address.domain.model.Customer> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.preDelete(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void postInsert(jp.co.demo.address.domain.model.Customer entity, org.seasar.doma.jdbc.entity.PostInsertContext<jp.co.demo.address.domain.model.Customer> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.postInsert(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void postUpdate(jp.co.demo.address.domain.model.Customer entity, org.seasar.doma.jdbc.entity.PostUpdateContext<jp.co.demo.address.domain.model.Customer> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.postUpdate(entity, context);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public void postDelete(jp.co.demo.address.domain.model.Customer entity, org.seasar.doma.jdbc.entity.PostDeleteContext<jp.co.demo.address.domain.model.Customer> context) {
        Class __listenerClass = org.seasar.doma.jdbc.entity.NullEntityListener.class;
        org.seasar.doma.jdbc.entity.NullEntityListener __listener = context.getConfig().getEntityListenerProvider().get(__listenerClass, __listenerSupplier);
        __listener.postDelete(entity, context);
    }

    @Override
    public java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<jp.co.demo.address.domain.model.Customer, ?>> getEntityPropertyTypes() {
        return __entityPropertyTypes;
    }

    @Override
    public org.seasar.doma.jdbc.entity.EntityPropertyType<jp.co.demo.address.domain.model.Customer, ?> getEntityPropertyType(String __name) {
        return __entityPropertyTypeMap.get(__name);
    }

    @Override
    public java.util.List<org.seasar.doma.jdbc.entity.EntityPropertyType<jp.co.demo.address.domain.model.Customer, ?>> getIdPropertyTypes() {
        return __idPropertyTypes;
    }

    @Override
    public org.seasar.doma.jdbc.entity.GeneratedIdPropertyType<java.lang.Object, jp.co.demo.address.domain.model.Customer, ?, ?> getGeneratedIdPropertyType() {
        return null;
    }

    @Override
    public org.seasar.doma.jdbc.entity.VersionPropertyType<java.lang.Object, jp.co.demo.address.domain.model.Customer, ?, ?> getVersionPropertyType() {
        return null;
    }

    @Override
    public jp.co.demo.address.domain.model.Customer newEntity(java.util.Map<String, org.seasar.doma.jdbc.entity.Property<jp.co.demo.address.domain.model.Customer, ?>> __args) {
        jp.co.demo.address.domain.model.Customer entity = new jp.co.demo.address.domain.model.Customer();
        __args.values().forEach(v -> v.save(entity));
        return entity;
    }

    @Override
    public Class<jp.co.demo.address.domain.model.Customer> getEntityClass() {
        return jp.co.demo.address.domain.model.Customer.class;
    }

    @Override
    public jp.co.demo.address.domain.model.Customer getOriginalStates(jp.co.demo.address.domain.model.Customer __entity) {
        return null;
    }

    @Override
    public void saveCurrentStates(jp.co.demo.address.domain.model.Customer __entity) {
    }

    /**
     * @return the singleton
     */
    public static _Customer getSingletonInternal() {
        return __singleton;
    }

    /**
     * @return the new instance
     */
    public static _Customer newInstance() {
        return new _Customer();
    }

    private static class ListenerHolder {
        private static org.seasar.doma.jdbc.entity.NullEntityListener<jp.co.demo.address.domain.model.Customer> listener = new org.seasar.doma.jdbc.entity.NullEntityListener<>();
    }

}
